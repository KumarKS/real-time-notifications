﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;

public class FCMclass : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
      {
          if (task.IsFaulted || task.IsCanceled)
          {
              Debug.Log("is faulted");
          }
          dependencyStatus = task.Result;
          if (dependencyStatus == Firebase.DependencyStatus.Available)
          {
              Debug.Log("Firebase cloud messaging Dependencies resolved");
              Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
              Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
          }
          else
          {
              Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
              // 						Firebase Unity SDK is not safe to use here.
          }
      });
    }

    private void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {

    }

    private void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
        FirebaseAPI.Instance.fcmToken = token.Token;
    }
}
