﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using FirebaseAuthAPI;
using System;

public class FirebaseAPI : Singleton<FirebaseAPI>
{
    private Action<string> OnVerifySuccess;
    private Action<string> OnVerifyError;
    public string fcmToken;
    private Dictionary<string, string> receiptDetails = new Dictionary<string, string>();
    public void Init()
    {
        UIHandler.Instance.OnUserSignInSuccess += OnUserSignInSuccess;
        UIHandler.Instance.OnUserSignInFail += OnUserSignInFail;
    }

    void OnDisable()
    {
        if (UIHandler.Instance != null)
        {
            UIHandler.Instance.OnUserSignInSuccess -= OnUserSignInSuccess;
            UIHandler.Instance.OnUserSignInFail -= OnUserSignInFail;
        }
    }
    private void OnUserSignInSuccess(bool s)
    {
#if UNITY_IOS
        StartCoroutine(ExecuteIosReceipt(OnVerifySuccess, OnVerifyError));
#elif UNITY_ANDROID
        StartCoroutine(ExecuteReceipt(OnVerifySuccess, OnVerifyError));
#endif
    }
    private void OnUserSignInFail(bool s)
    {
        Debug.LogError("OnUserSignInFail" + s);
    }

    public void VerifyTheGoogleReceipt(Action<string> onVerifySuccess, Action<string> onVerifyError, string subID, string purToken)
    {
        receiptDetails.Clear();
        receiptDetails.Add("subId", subID);
        receiptDetails.Add("purToken", purToken);

        if (UIHandler.Instance.auth == null || UIHandler.Instance.auth.CurrentUser == null)
        {
            OnVerifySuccess += onVerifySuccess;
            OnVerifyError += onVerifyError;

            UIHandler.Instance.SigninAnonymouslyAsync();
        }
        else
        {
            StartCoroutine(ExecuteReceipt(onVerifySuccess, onVerifyError));
        }
    }

    private IEnumerator ExecuteReceipt(Action<string> onVerifySuccess, Action<string> onVerifyError)
    {
        UnityWebRequest www = new UnityWebRequest("https://us-central1-kidzooly-learning-app.cloudfunctions.net/VerifyGoogleReceipt", "POST");

        receiptDetails.Add("uid", UIHandler.Instance.auth.CurrentUser.UserId);
        receiptDetails.Add("dToken", fcmToken);
        receiptDetails.Add("mail_Id", UIHandler.Instance.auth.CurrentUser.Email);

        var kvp = receiptDetails;

        var datatobesend = JsonConvert.SerializeObject(kvp);
        byte[] bodyrow = Encoding.UTF8.GetBytes(datatobesend);
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyrow);
        www.chunkedTransfer = false;
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("content-type", "Application/json");
        using (www)
        {
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch failed. " + www.error);
                onVerifyError(www.error);
            }
            else
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch success" + www.downloadHandler.text);
                var lbs = www.downloadHandler.text;
                onVerifySuccess(lbs);
            }
        }
    }

    public void VerifyTheIOSReceipt(Action<string> onVerifySuccess, Action<string> onVerifyError)
    {
        if (UIHandler.Instance.auth == null || UIHandler.Instance.auth.CurrentUser == null)
        {
            OnVerifySuccess += onVerifySuccess;
            OnVerifyError += onVerifyError;

            UIHandler.Instance.SigninAnonymouslyAsync();
        }
        else
        {
            StartCoroutine(ExecuteIosReceipt(onVerifySuccess, onVerifyError));
        }
    }

    private IEnumerator ExecuteIosReceipt(Action<string> onVerifySuccess, Action<string> onVerifyError)
    {
        UnityWebRequest www = new UnityWebRequest("https://us-central1-kidzooly-learning-app.cloudfunctions.net/AppleSubscriptionValidation", "POST");
        var kvp = new Dictionary<string, string>() {{ "latest_receipt", "ewoJInNpZ25hdHVyZSIgPSAiQXlWYkpPYlF5UEovNCs4bk1zRHNxMkE3K2lidDNrc0JHNjZDSml5UFRwTmEzWW9YbkJWMXVmMW1LbVFmRmJWc2EzdXZqa1M0UllQbmh5V0wvU1VDMWlGTGxxS1FzTDF2TXhkaGxrczNxSmRka1hCZjlGVWhyajh0cTJEWkp3K3pPVk9xVC9sVjNCbXRDM01UOXpQSklobjlUczR6T21ZM2NkdjVxNVdCRlRtaXMvVDZuQ2paUDhCd29aaVdVeWlLb241TldPY1hUczB0YUJJK2UwemRLQUJMM1VBZUUxSFQ0bWdIdmd3bmRaNWhMRThFUHdtN0lBSnVkNVIrQTNsWW5NcFV0OUVIZHFYNDRBd28rNW5zcUw0Z2FFakRNQmk4UUNaVHAzaXg1bzAwb3dHd0F6dUVSVU50QVBWQUl5aDByZitoRWlqR0ZHV2ZjYjM1bFZLVFJZMEFBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ERTVMVEF4TFRBNUlEQXhPakkyT2pNMUlFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5GMVlXNTBhWFI1SWlBOUlDSXhJanNLQ1NKMWJtbHhkV1V0ZG1WdVpHOXlMV2xrWlc1MGFXWnBaWElpSUQwZ0lrWkJOVE00T1RaQ0xVSXhSVGt0TkRBNU5pMDVRekEyTFRjM01FTTRPRFl5UVRnMk5TSTdDZ2tpYjNKcFoybHVZV3d0Y0hWeVkyaGhjMlV0WkdGMFpTMXRjeUlnUFNBaU1UVTBOekF5TlRrNU5UQXdNQ0k3Q2draVpYaHdhWEpsY3kxa1lYUmxMV1p2Y20xaGRIUmxaQ0lnUFNBaU1qQXhPUzB3TnkweU9TQXdPVG8xT1Rvd01DQkZkR012UjAxVUlqc0tDU0pwY3kxcGJpMXBiblJ5YnkxdlptWmxjaTF3WlhKcGIyUWlJRDBnSW1aaGJITmxJanNLQ1NKd2RYSmphR0Z6WlMxa1lYUmxMVzF6SWlBOUlDSXhOVFkwTXprME1UWXdNREF3SWpzS0NTSmxlSEJwY21WekxXUmhkR1V0Wm05eWJXRjBkR1ZrTFhCemRDSWdQU0FpTWpBeE9TMHdOeTB5T1NBd01qbzFPVG93TUNCQmJXVnlhV05oTDB4dmMxOUJibWRsYkdWeklqc0tDU0pwY3kxMGNtbGhiQzF3WlhKcGIyUWlJRDBnSW1aaGJITmxJanNLQ1NKcGRHVnRMV2xrSWlBOUlDSXhNams0TkRNMU1UYzNJanNLQ1NKMWJtbHhkV1V0YVdSbGJuUnBabWxsY2lJZ1BTQWlaVEZsWkRRd09UZ3lNVGRqTW1Oak1ERTVZVFk0Tm1Fek1USTBZamt3WVRReE9XVmpNRFppTmlJN0Nna2liM0pwWjJsdVlXd3RkSEpoYm5OaFkzUnBiMjR0YVdRaUlEMGdJakV3TURBd01EQTBPVEk0TWpNeE5UZ2lPd29KSW1WNGNHbHlaWE10WkdGMFpTSWdQU0FpTVRVMk5ETTVORE0wTURBd01DSTdDZ2tpZEhKaGJuTmhZM1JwYjI0dGFXUWlJRDBnSWpFd01EQXdNREExTlRFM09Ea3lOekVpT3dvSkltSjJjbk1pSUQwZ0lqVTJJanNLQ1NKM1pXSXRiM0prWlhJdGJHbHVaUzFwZEdWdExXbGtJaUE5SUNJeE1EQXdNREF3TURRMU9USXlNREV5SWpzS0NTSjJaWEp6YVc5dUxXVjRkR1Z5Ym1Gc0xXbGtaVzUwYVdacFpYSWlJRDBnSWpBaU93b0pJbUpwWkNJZ1BTQWlZMjl0TG10cFpIcHZiMng1TG10cFpITXRiblZ5YzJWeWVTMXlhSGx0WlhNdGMzUnZjbWxsY3kxc1pXRnlibWx1WnlJN0Nna2ljSEp2WkhWamRDMXBaQ0lnUFNBaVMybGtlbTl2YkhrdWMzVmljMk55YVhCMGFXOXVMakYzWldWcklqc0tDU0p3ZFhKamFHRnpaUzFrWVhSbElpQTlJQ0l5TURFNUxUQTNMVEk1SURBNU9qVTJPakF3SUVWMFl5OUhUVlFpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0Y0hOMElpQTlJQ0l5TURFNUxUQTNMVEk1SURBeU9qVTJPakF3SUVGdFpYSnBZMkV2VEc5elgwRnVaMlZzWlhNaU" },
        { "uid", UIHandler.Instance.auth.CurrentUser.UserId }, {"mail_Id", UIHandler.Instance.auth.CurrentUser.Email} };
        var datatobesend = JsonConvert.SerializeObject(kvp);
        byte[] bodyrow = Encoding.UTF8.GetBytes(datatobesend);
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyrow);
        www.chunkedTransfer = false;
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("content-type", "Application/json");
        using (www)
        {
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch failed. " + www.error);
                onVerifyError(www.error);
            }
            else
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch success" + www.downloadHandler.text);
                var lbs = www.downloadHandler.text;
                onVerifySuccess(lbs);
            }
        }
    }

    private IEnumerator GetSubscriptionStatus(Action<string> onGetSuccess, Action<string> onGetError)
    {
        UnityWebRequest www = new UnityWebRequest("https://us-central1-kidzooly-learning-app.cloudfunctions.net/GetSubscriptionStatus", "POST");
        var kvp = new Dictionary<string, string>() { { "uid", UIHandler.Instance.auth.CurrentUser.UserId } };
        var datatobesend = JsonConvert.SerializeObject(kvp);
        byte[] bodyrow = Encoding.UTF8.GetBytes(datatobesend);
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyrow);
        www.chunkedTransfer = false;
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("content-type", "Application/json");
        using (www)
        {
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch failed. " + www.error);
                onGetError(www.error);
            }
            else
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch success" + www.downloadHandler.text);
                var lbs = www.downloadHandler.text;
                onGetSuccess(lbs);
            }
        }
    }

    public void CheckUserExits(Action<string> onGetSuccess, string purToken)
    {
        StartCoroutine(CheckUserAlreadyExists(onGetSuccess, purToken));
    }
    private IEnumerator CheckUserAlreadyExists(Action<string> onGetSuccess, string purToken)
    {
        UnityWebRequest www = new UnityWebRequest("https://us-central1-kidzooly-learning-app.cloudfunctions.net/GetUserIdUsingThePurahaseToken", "POST");
        var kvp = new Dictionary<string, string>() { { "pt", purToken } };
        var datatobesend = JsonConvert.SerializeObject(kvp);
        byte[] bodyrow = Encoding.UTF8.GetBytes(datatobesend);
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyrow);
        www.chunkedTransfer = false;
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("content-type", "Application/json");
        using (www)
        {
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch failed. " + www.error);
                // onGetError(www.error);
            }
            else
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch success" + www.downloadHandler.text);
                var lbs = www.downloadHandler.text;
                onGetSuccess(lbs);
            }
        }
    }

    public void OpenTheSubscriptionPanel()
    {
        Application.OpenURL("http://play.google.com/store/account/subscriptions");
    }

    public void GetTheUserCustomToken(Action<string> onGetSuccess, string uid)
    {
        StartCoroutine(GetTheCustomToken(onGetSuccess, uid));
    }
    private IEnumerator GetTheCustomToken(Action<string> onGetSuccess, string uid)
    {
        UnityWebRequest www = new UnityWebRequest("https://us-central1-kidzooly-learning-app.cloudfunctions.net/CreateCustomToken", "POST");
        var kvp = new Dictionary<string, string>() { { "uid", uid } };
        var datatobesend = JsonConvert.SerializeObject(kvp);
        byte[] bodyrow = Encoding.UTF8.GetBytes(datatobesend);
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyrow);
        www.chunkedTransfer = false;
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("content-type", "Application/json");
        using (www)
        {
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch failed. " + www.error);
                // onGetError(www.error);
            }
            else
            {
                Debug.Log("[Firebase Leaderboard] Leaderboard fetch success" + www.downloadHandler.text);
                var lbs = www.downloadHandler.text;
                onGetSuccess(lbs);
            }
        }
    }

}
