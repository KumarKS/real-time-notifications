﻿using UnityEngine;
using System.Collections;
using UnityEngine.Purchasing;
using System;
using System.Collections.Generic;

//using Fabric.Answers;
using System.IO;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using FirebaseAuthAPI;

public class SoomlaHandler : Singleton<SoomlaHandler>, IStoreListener
{

    private string itemToBuy = "kidzooly.subscription.1week";
    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    public bool isRestoreButtonClicked;

    public void Init()
    {
        if (m_StoreController == null)
            InitializePurchasing();
    }

    public void InitializePurchasing()
    {
        Debug.Log("initialized");
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());


        // for (int i = 0; i < MyAssets.VirtualGoods.Length; i++)
        // {
        builder.AddProduct(itemToBuy, ProductType.NonConsumable);
        //     LogToGUI.Log(MyAssets.VirtualGoods[i].itemId);
        // }

        UnityPurchasing.Initialize(this, builder);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        //LogToGUI.Log("OnInitialized: PASS");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
        Debug.Log("ON INIT");
        // for (int i = 0; i < MyAssets.VirtualGoods.Length; i++)
        // {
        Product pvi = m_StoreController.products.WithID(itemToBuy);
        if (pvi != null)
        {
            Debug.Log(pvi.metadata.localizedDescription + "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Debug.Log(pvi.receipt + " $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ " + pvi.transactionID);
            if (pvi.hasReceipt)
            {
                Debug.Log("Got receipet" + pvi.receipt);
                // LocalStorage.SetAdsPurchased(true);
                AfterItemPurchase();
                // break;
            }
            else
            {
                Debug.Log("Got no receipet");
            }
            // LogToGUI.Log(MyAssets.VirtualGoods[i].name);
            // MyAssets.VirtualGoods[i].UpdatePricing(pvi.metadata.localizedPriceString);
            // LogToGUI.Log(pvi.metadata.localizedTitle + " " + pvi.metadata.isoCurrencyCode + " " + pvi.metadata.localizedPriceString);
        }
        // }

        // PurchasePanel.Instance.UpdateProperties();

        //LogToGUI.Log("******************************IAP************************");
        //LogToGUI.Log(m_StoreController != null);
        //LogToGUI.Log(m_StoreExtensionProvider != null);
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyProductID(string itemID)
    {
        // LocalStorage.UpdatePurchaseAttempts();
        itemID = itemToBuy;
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(itemID);

            // LogToGUI.Log("*************" + itemID + "initialized");
            if (product != null && product.availableToPurchase)
            {
                // LogToGUI.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
                // LogToGUI.Log(itemID);
            }
            else
            {
                // LogToGUI.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            // LogToGUI.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    private void DebugBuyProductId(string itemID)
    {
        // LocalStorage.SetAdsPurchased(true);
        AfterItemPurchase();
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            // LogToGUI.Log("*********** not initialized");
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            // LogToGUI.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }
        // LogToGUI.Log("************ initialized");
        isRestoreButtonClicked = true;
        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            // LogToGUI.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                if (result)
                {
                    // LogToGUI.Log("$$$$$$ restoration completed $$$$$$$");
                    AfterItemPurchase();
                }
                else
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    // LogToGUI.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                }
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            // DialogBoxCreator.Instance.CreateNewInfoBox("Restoration failed", "Info");
            // // LogToGUI.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    // 	public void OnPurchaseFailed (Product i, PurchaseFailureReason p)
    // 	{
    // 		//LogToGUI.Log("MarketPurchasefailed: " + i.metadata.localizedTitle + "  Reason: " + p.ToString());
    // 		LocalStorage.SetFailedPurchaseItemID (i.metadata.localizedTitle);
    // 		Dictionary<string, object> dict = new Dictionary<string, object> ();
    // 		dict.Add ("Reason", (object)p.ToString ());
    // //		Debug.Log ("Am coming");
    // //        Answers.LogPurchase(i.metadata.localizedPrice, i.metadata.isoCurrencyCode, false, i.metadata.localizedTitle, i.definition.type.ToString(), i.definition.id, dict);
    // 		LocalStorage.UpdatePurchaseFailures ();
    // 	}

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        if (failureReason == PurchaseFailureReason.DuplicateTransaction)
        {
            Debug.Log("Failed reason " + product.definition.id);
            if (product.definition.id == "no_ads_animinies" || product.definition.id == "no_ads_animinies_50")
            {
                AfterItemPurchase();
            }
            else
            {
                //LogToGUI.Log("MarketPurchasefailed: " + i.metadata.localizedTitle + "  Reason: " + p.ToString());
                // LocalStorage.SetFailedPurchaseItemID(product.metadata.localizedTitle);
                Dictionary<string, object> dict = new Dictionary<string, object>();
                dict.Add("Reason", (object)failureReason.ToString());
                // LocalStorage.UpdatePurchaseFailures();
            }
        }
    }

    public void OnUnexpectedStoreError()
    {
        // LogToGUI.Log("OnUnexpectedStoreError");
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log("OnMarketPurchase: " + args.purchasedProduct.definition.id);
        Debug.Log("OnMarketPurchase receipt: " + args.purchasedProduct.receipt);
        // LocalStorage.SetFailedPurchaseItemID("");
        // LogToGUI.Log("zxxxxxxxxx");

        Product purchasedProduct = args.purchasedProduct;

        if (purchasedProduct != null)
        {
            var wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(args.purchasedProduct.receipt);
            var payload = (string)wrapper["Payload"];
            var gpDetails = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
            var gpJson = (string)gpDetails["json"];
            var gpDetails1 = (Dictionary<string, object>)MiniJson.JsonDecode(gpJson);
            string pToken = gpDetails1["purchaseToken"].ToString();

            FirebaseAPI.Instance.CheckUserExits(
                    (success) =>
                    {
                        var receivedUid = (Dictionary<string, object>)MiniJson.JsonDecode(success);
                        string olduid = (string)receivedUid["uid"];

                        if (olduid == null)
                        {
                            FirebaseAPI.Instance.VerifyTheGoogleReceipt(
                                (s) => { Debug.Log("OnMarketPurchase receipt s : " + s); },

                                (f) => { Debug.Log("OnMarketPurchase receipt f: " + f); },

                                args.purchasedProduct.definition.id, pToken);
                        } else 
                        {
                            UIHandler.Instance.DebugLog("USER already exists : "+ olduid);
                            FirebaseAPI.Instance.GetTheUserCustomToken((s)=> {
                                    Debug.Log("GetTheUserCustomToken: "+s);
                                    UIHandler.Instance.SignInWithCustomToken(s);
                            }, olduid);

                        }
                    },
                    pToken
            );


            // LogToGUI.Log("cvvvvvvvvv");
            //            Answers.LogPurchase(purchasedProduct.metadata.localizedPrice, purchasedProduct.metadata.isoCurrencyCode, true, purchasedProduct.metadata.localizedTitle, purchasedProduct.definition.type.ToString(), purchasedProduct.definition.id);
        }
        AfterItemPurchase();
        ItemPurchased = true;
        // RateUsProcess.Instance.OnStartRatingProcess();
        return PurchaseProcessingResult.Complete;
    }

    bool ItemPurchased = false;

    public void AfterItemPurchase()
    {
        // MediaPlayerUI.Instance.player.GetComponent<Animator>().SetBool("FULLSRC", true);
        // LocalStorage.SetAdsPurchased(true);
        // PurchasePanel.Instance.OnAdsPurchased();
        // Adsmanager.Instance.OnPurchase();
#if !UNITY_IOS
        // GiftTimer.Instance.OnItemPurchase();
#endif
    }

}

